import axios from 'axios'

export function login(data){
    return axios({
        url:'auth/login',
        method:'post',
        data:data
    })
}

export function getUser(){
    return axios({
        url:'auth/getUser',
        method:'get'
    })
}

export function basicUserMsg(token){
    return axios({
        url:'auth/basicUserMsg',
        method:'get',
        headers: {
            'Content-Type': 'application/json',
            'token': token
        }
    })
}

export function renewUser(data){
    return axios({
        url:'auth/renewUser',
        method:'post',
        data
    })
}

export function getVerificationCode(data){
    return axios({
        url:'auth/verificationCode',
        method:'post',
        data
    })
}
