import axios from 'axios'

export function showSlide(){
    return axios({
        url:'banner/showSlide',
        method:'get'
    })
}

export function renewSlide(data){
    return axios({
        url:'banner/renewSlide',
        method:'post',
        data
    })
}
