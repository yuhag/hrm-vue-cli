import axios from 'axios'

export function company(){
    return axios({
        url:'company',
        method:'get'
    })
}

export function edit(data){
    return axios({
        url:'company/edit',
        method:'post',
        data
    })
}

