import axios from 'axios'


export function deptAndPost(){
    return axios({
        url:'dept',
        method:'get'
    })
}

export function organization(){
    return axios({
        url:'dept/organization',
        method:'get'
    })
}

export function insertPositions(data){
    return axios({
        url:'/dept/addPositions',
        method:'post',
        data
    })
}

export function getDeptAndPosition(){
    return axios({
        url:'dept/getDeptAndPosition',
        method:'get'
    })
}

export function empAmount(position){
    return axios({
        url:'dept/empAmount',
        method:'get',
        params:{
            position
        }
    })
}

export function deleteDept(id){
    return axios({
        url:`dept/deleteDept/${id}`,
        method:'delete'
    })
}


export function updateDept(data){
    return axios({
        url:`dept/updateDept`,
        method:'put',
        data
    })
}

export function treeOrganization(){
    return axios({
        url:`dept/treeOrganization`,
        method:'get'
    })
}
