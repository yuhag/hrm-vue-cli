import axios from 'axios'

export function pagePersonnel(current,pageSize,data){
    return axios({
        url:'personnel/pagePersonnel',
        method:'get',
        params:{
            current,
            pageSize,
            identifier:data.identifier,
            reviewerName:data.reviewerName,
            name:data.name,
            graduationSchool:data.graduationSchool,
            expectPosition:data.expectPosition
        }
    })
}

export function searchPersonnel(data){
    return axios({
        url:'personnel/searchPersonnel',
        method:'get',
        params:{
            identifier:data.identifier,
            name:data.name
        }
    })
}

export function cardPersonnel(){
    return axios({
        url:'personnel/cardPersonnel',
        method:'get'
    })
}

export function accordingTo(id){
    return axios({
        url:`personnel/accordingTo/${id}`,
        method:'get'
    })
}

export function addAudit(data,token){
    return axios({
        url:'personnelStatus/addAudit',
        method:'post',
        data,
        headers: {
            'Content-Type': 'application/json',
            'token': token
        }
    })
}

export function viewLocation(id){
    return axios({
        url:`personnelStatus/viewLocation/${id}`,
        method:'get'
    })
}

export function showStage(id){
    return axios({
        url:`personnelStatus/showStage/${id}`,
        method:'get'
    })
}

export function emailSend(data,id){
    return axios({
        url:`personnelStatus/emailSend/${id}`,
        method:'post',
        data
    })
}




