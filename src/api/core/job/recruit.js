import axios from 'axios'


export function saveRecruit(data){
    return axios({
        url:'recruit/saveRecruit',
        method:'post',
        data,
        headers: {
            'Content-Type': 'application/json',
            'token': data.token
        },
    })
}

export function pageRecruit(current,pageSize,data){
    return axios({
        url:'recruit/pageRecruit',
        method:'get',
        params: {
            current,
            pageSize,
            deptId: data.deptId,
            postId: data.postId,
            userId: data.userId,
            minSalary:data.minSalary,
            maxSalary:data.maxSalary,
            startTime: data.startTime,
            endTime: data.endTime
        }
    })
}

export function delRecruit(id){
    return axios({
        url:`recruit/delRecruit/${id}`,
        method:'delete'
    })
}


export function getRecruitById(id){
    return axios({
        url:`recruit/getRecruitById/${id}`,
        method:'get'
    })
}

export function updateRecruit(data){
    return axios({
        url:'recruit/editRecruit',
        method:'put',
        data
    })
}
