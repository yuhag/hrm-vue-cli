import axios from 'axios'

export function dept_boss(){
    return axios({
        url:'emp/boss',
        method:'get'
    })
}

export function addEmp(data){
    return axios({
        url:'emp/add',
        method:'post',
        data
    })
}

export function getEmpAll(pageSize,current){
    return axios({
        url:'emp',
        method:'get',
        params:{
            pageSize,
            current
        }
    })
}

export function updateEmp(data){
    return axios({
        url:'emp',
        method:'put',
        data
    })
}

export function deleteEmp(id){
    return axios({
        url:`emp/${id}`,
        method:'delete'
    })
}

export function notLeader(){
    return axios({
        url:'emp/notLeader',
        method:'get'
    })
}

export function selectById(id){
    return axios({
        url:`emp/${id}`,
        method:'get'
    })
}
