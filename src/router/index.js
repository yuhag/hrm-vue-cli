import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/from',
  },
  {
    path: '/home',
    name: 'Home',
    component: () => import('@/views/login/Home'),
  },
  {
    path: '/from',
    name: 'From',
    component: () => import('@/views/from/index')
  },

    //人员管理
  {
    path: '/personnel',
    name: 'Personnel',
    component: () => import('@/views/personnel/index')
  },

   //组织机构
  {
    path: '/organization',
    name: 'Organization',
    component: () => import('@/views/organization/Begin')
  },

  //招聘管理
  {
    path: '/recruit',
    name: 'Recruit',
    component: () => import('@/views/recruit/Recruit')
  },
  {
    path: '/employee',
    name: 'Employee',
    component: () => import('@/views/recruit/Employee')
  },
  {
    path: '/talentDetails',
    name: 'TalentDetails',
    component: () => import('@/views/recruit/TalentDetails')
  },
  {
    path: '/journey/:id',
    name: 'Journey',
    component: () => import('@/views/recruit/Journey')
  },
  {
    path: '/report',
    name: 'Report',
    component: () => import('@/views/recruit/Report')
  },

  //系统设置
  {
    path: '/corporation',
    name: 'Corporation',
    component: () => import('@/views/system/Corporation')
  },
  {
    path: '/slide',
    name: 'Slide',
    component: () => import('@/views/system/Slide')
  },
  {
    path: '/user',
    name: 'User',
    component: () => import('@/views/system/User')
  }







]

const router = new VueRouter({
  base: process.env.BASE_URL,
  routes
})

export default router
