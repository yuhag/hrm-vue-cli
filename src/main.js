import Vue from 'vue'
import App from './App.vue'
import router from './router'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import axios from 'axios';
import Vue2OrgTree from 'vue-tree-color';
import Vue2rgTree from 'vue2-org-tree'
import "bootstrap/dist/css/bootstrap.css"
import vueToPdf from 'vue-to-pdf';
import VueCookies from 'vue-cookies'

Vue.config.productionTip = false
Vue.use(ElementUI);
Vue.use(Vue2rgTree);
Vue.use(vueToPdf);
Vue.use(VueCookies)


//42.192.141.79
axios.defaults.baseURL = "http://localhost:9000/api"
Vue.prototype.$axios = axios


new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
